//! CFPx is a Rust application designed to manage your
//! conference's call for proposals, programs, and
//! schedule.

#![allow(dead_code, unused_imports)]
#![feature(await_macro, async_await)]

mod github;
mod handlers;
mod querystring;
mod repo;

use crate::{github::GitHub, handlers::auth, repo::Repo};
use lazy_static::lazy_static;
use surf::middlewares::{cors::CorsBlanket, log::RequestLogger};

lazy_static! {
    static ref DATABASE_URL: String = std::env::var("DATABASE_URL").unwrap_or("".to_string());
    static ref CLIENT_ID: String = std::env::var("GITHUB_CLIENT_ID").unwrap_or("".to_string());
    static ref CLIENT_SECRET: String = std::env::var("GITHUB_CLIENT_ID").unwrap_or("".to_string());
}

/// Convenience function for building an instance
/// of the CFP application from provided environment
/// variables, panicking if errors occur.
pub fn app() -> tide::App<Repo> {
    dotenv::dotenv().expect("Unable to retrieve env variables");

    let oauth = GitHub::new(CLIENT_ID.to_string(), CLIENT_SECRET.to_string())
        .expect("Unable to initialize OAuth2 client");

    let repo = Repo::new(DATABASE_URL.as_ref(), oauth).expect("Unable to initialize repo");

    let mut app = tide::App::new(repo);

    app.middleware(RequestLogger::new());
    app.middleware(CorsBlanket::new());

    app.at("/").get(handlers::index);
    app.at("/login").get(auth::login);
    app.at("/oauth/callback").get(auth::callback);

    app
}
