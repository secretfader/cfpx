# Development Workflow

1. Clone the repository by running `git clone
   https://gitlab.com/secretfader/cfpx`

2. Run `cargo make db`, which will spin up an instance of Postgres 11 with
   Podman.

3. Run `cargo make run` to build and run the application.
