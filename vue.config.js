const path = require('path')

module.exports = {
  chainWebpack: config => {
    config.entry('app')
      .clear()
      .add(path.resolve(__dirname, 'app/main.ts'))
      .end()
    config.resolve.alias.set('@', path.join(__dirname, 'app'))
  },
}
