import Vue from 'vue';
import Router from 'vue-router';
import Bootstrap from 'bootstrap-vue';

import App from './App.vue';
import Home from './views/Home.vue';

Vue.config.productionTip = false;
Vue.use(Bootstrap);
Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue'),
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
    },
  ],
});

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
