use crate::querystring::ExtractQuery;
use crate::repo::Repo;
use http_service::Body;
use hyper::header::LOCATION;
use serde_derive::Deserialize;
use tide::error::Error;

/// Redirect client to GitHub and launch OAuth2
/// authentication flow.
pub async fn login(repo: tide::Context<Repo>) -> http::Response<Body> {
    let (authorize_url, _) = repo.app_data().oauth_client().authorize_url();

    http::Response::builder()
        .status(302)
        .header(LOCATION, authorize_url.as_str())
        .body(Body::empty())
        .unwrap()
}

/// Callback to process incoming OAuth2 params.
pub async fn callback(cx: tide::Context<Repo>) -> Result<http::Response<Body>, Error> {
    let _ = cx.query::<OAuth2Params>()?;

    Ok(http::Response::builder()
        .status(200)
        .body(Body::empty())
        .unwrap())
}

#[derive(Deserialize, Debug)]
struct OAuth2Params {
    code: String,
    state: String,
}
