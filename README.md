# CFPx

[![GitLab CI Pipeline Status](https://gitlab.com/secretfader/cfpx/badges/master/pipeline.svg)](https://gitlab.com/secretfader/cfpx/commits/master)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0)
[![Made by Fader](https://img.shields.io/badge/made_by-Fader-purple.svg)](https://www.secretfader.com)

A Rust application designed to manage your conference's call for proposals, program, and schedule. **Under heavy development. Use at your own risk.**

## Developing

Ensure that [Podman](https://podman.io) (or [Docker](https://docker.io)),
[Node.js](https://nodejs.org), and [Rust](https://rust-lang.org) are
installed and working correctly, then read about the [application
architecture](docs/architecture.md).

Most of the normal development workflows are outlined in
[Makefile.toml](Makefile.toml), but you should also read the [workflow
docs](docs/workflow.md) for details.

If your development question isn't sufficiently explained in these documents,
please open an issue.

## Why "CFPx"?

"CFP" is a common term meaning "Call For Proposals." Conference organizers
"open" their CFP in order to solicit content ideas from potential speakers.
This repo contains an "extended" or "experimental" platform for gathering
proposals, so we call it CFPx.

When CFPx migrates to a stable release schedule (instead of relying on Rust nightly and the `master` branch of most dependencies), then we can consider dropping the "x".

## License

Copyright 2019 Nicholas Young. All rights reserved. Released under the [Apache
License, version 2.0](LICENSE).
