import { shallowMount } from '@vue/test-utils';
import HelloWorld from '../HelloWorld.vue';

describe('HelloWorld.vue', () => {
  test('renders', () => {
    const wrapper = shallowMount(HelloWorld);
    expect(wrapper.text()).toBeDefined();
  });
});
