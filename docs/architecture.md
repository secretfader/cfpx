# Architecture of CFPx

Welcome to the technical documentation covering the various systems that
comprise CFPx. This document is intended to answer basic questions about
developing in the repository environment.

If you have a question that isn't answered here but should be, open an issue.

## Backend

The application backend is written in Rust, with source code residing in
[`src/`](../src). It serves [JSON API](https://jsonapi.org)-formatted responses
over HTTP using [Tide](https://github.com/rustasync/tide), one of the newer
Rust web frameworks, developed by the Async Networking Working Group. Data is
stored in [PostgreSQL](https://www.postgresql.org) with
[Diesel](https://diesel.rs).

Since many of the technologies used in the backend are still in a state of
technical preview as of this writing, compiling the backend service requires
Rust nightly (see [`src/lib.rs`](../src/lib.rs) for details).

## Frontend

CFPx's frontend is written in [Vue.js](https://vuejs.org) and
[TypeScript](https://typescriptlang.org). The combination of Vue and TypeScript was chosen for
a number of reasons.

First, since Vue bills itself as a "progressive" framework,
server side rendering is a snap. Second, the HTML-based templating language
should be familiar to most contributors. Lastly, it is mildly
functional, taking inspiration from Elm (and other
pure-functional frontend frameworks) where appropriate.

TypeScript was chosen because it's a familiar tool that merges nicely with
Rust. Styles are powered by [PostCSS](https://postcss.org).

## Setup

In order to develop CFPx on your local machine, you'll need the following
dependencies installed:

* [Rust](https://rust-lang.org) (tested on 2019-03-24 nightly, and newer)
* [Podman](https://podman.io) (or [Docker](https://docker.io))

Additionally, this repository makes use of [`cargo
make`](https://github.com/sagiegurari/cargo-make) and
[`diesel_cli`](https://github.com/diesel-rs/diesel/tree/master/diesel_cli)
command line tools. Install them by running `cargo install
cargo-make --force` and `cargo install diesel_cli
--features=postgres --force`.

Once all of the development tools are installed, PostgreSQL
can be started by running `cargo make db`.
