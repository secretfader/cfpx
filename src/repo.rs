use crate::github::GitHub;
use diesel::{pg::PgConnection, r2d2::ConnectionManager};
use futures_stable::future::poll_fn;
use r2d2::{Error, Pool, PooledConnection};
use tokio_threadpool::blocking;

/// Type alias for connections retrieved from the pool.
pub type Connection = PooledConnection<ConnectionManager<PgConnection>>;

/// Type alias for pool of Postgres connections.
pub type ConnectionPool = Pool<ConnectionManager<PgConnection>>;

/// Wrapper type for retrieving records from the
/// database.
#[derive(Clone)]
pub struct Repo {
    pool: ConnectionPool,
    github: GitHub,
}

impl Repo {
    /// Construct a new instance of Repo from a provided
    /// URL pointing to the database.
    pub fn new(url: &'static str, github: GitHub) -> Result<Self, Error> {
        let manager = ConnectionManager::new(url);
        let pool = Pool::new(manager)?;
        Ok(Self { pool, github })
    }

    pub fn oauth_client(&self) -> GitHub {
        self.github.clone()
    }
}

/// Run database queries wrapped in the provided
/// closure (Note: portions of this API require
/// `futures = 0.1` while others require `await!`
/// syntax additions).
pub async fn run_with_diesel<F, T>(repo: &Repo, f: F) -> T
where
    F: FnOnce(Connection) -> T + Send + std::marker::Unpin + 'static,
    T: Send + 'static,
{
    let pool = repo.pool.clone();
    let mut f = Some(f);
    tokio::await!(poll_fn(|| blocking(|| (f.take().unwrap())(
        pool.get().unwrap()
    ))
    .map_err(|_| panic!("the threadpool shut down"))))
    .expect("Error running async database task.")
}
