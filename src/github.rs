//! Adapter for GitHub's OAuth2 API, or at least the
//! portions required for authentication.
use oauth2::{
    basic::BasicClient, prelude::*, AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken,
    RedirectUrl, Scope, TokenUrl,
};
use url::{ParseError, Url};

/// Wrapper around the OAuth2 client that commmunicates
/// with GitHub's API.
#[derive(Clone)]
pub struct GitHub(pub BasicClient);

impl GitHub {
    pub fn new(id: String, secret: String) -> Result<Self, ParseError> {
        let client_id = ClientId::new(id);
        let client_secret = ClientSecret::new(secret);
        let auth_url = AuthUrl::new(Url::parse("https://github.com/login/oauth/authorize")?);
        let token_url = TokenUrl::new(Url::parse("https://github.com/login/oauth/access_token")?);

        let client = BasicClient::new(client_id, Some(client_secret), auth_url, Some(token_url))
            .add_scope(Scope::new("user:email".to_string()))
            .set_redirect_url(RedirectUrl::new(Url::parse("http://localhost:8080")?));

        Ok(Self(client))
    }

    /// Retrieve a new authorization URL from the
    /// underlying OAuth2 client.
    pub fn authorize_url(&self) -> (Url, CsrfToken) {
        self.0.authorize_url(CsrfToken::new_random)
    }
}
