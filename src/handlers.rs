use crate::repo::Repo;
use http_service::Body;

pub mod auth;

/// Request handler for `/`. Generally, this handler won't
/// be used at all, but rather redirect to the current
/// event's CFP submissions page.
pub async fn index(_: tide::Context<Repo>) -> http::Response<Body> {
    http::Response::builder()
        .status(200)
        .body(Body::empty())
        .unwrap()
}
