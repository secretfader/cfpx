fn main() {
    let app = cfpx::app();
    let addr: std::net::SocketAddr = std::env::var("HOST")
        .unwrap_or("127.0.0.1:7878".to_string())
        .parse()
        .unwrap();

    app.serve(addr).expect("Unable to start CFP app");
}
