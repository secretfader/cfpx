use futures::future::FutureObj;
use http::StatusCode;
use serde::Deserialize;
use tide::{
    error::Error,
    middleware::{Middleware, Next},
    Context,
};

/// Provides querystring deserialization functionality to
/// Tide endpoints, returning an error if the requested
/// operation fails.
pub trait ExtractQuery<'de> {
    fn query<T: Deserialize<'de>>(&'de self) -> Result<T, Error>;
}

impl<'de, Data> ExtractQuery<'de> for Context<Data> {
    fn query<T: Deserialize<'de>>(&'de self) -> Result<T, Error> {
        let query = self.uri().query();

        if query.is_none() {
            return Err(Error::from(StatusCode::BAD_REQUEST));
        }

        Ok(serde_urlencoded::from_str(query.unwrap())
            .map_err(|_| Error::from(StatusCode::BAD_REQUEST))?)
    }
}
